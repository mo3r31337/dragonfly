Installation notes
==================

> **Important difference between soapUI and ReadyAPI**
>
> With the release soapUI 5.12 and ReadyAPI, Smartbear changed the plugin architecture. As such, there are some minor differences that users will experience when comparing their experience with soapUI 4.x and 5.0x (legacy plugin architecture) vs. soapUI 5.12 and ReadyAPI (new plugin architecture).
> 
> Here are the main differences:

---

> **Installation Directory for soapUI 5.12 and ReadyAPI**
> 
> In the new plugin architecture, Smartbear requires that all plugins are installed in
>
> `user.home\.soapui\plugins`  
>
> **example:** `C:\\Users\agiletestware\.soapui\plugins`
>
> The dragonfly java based installer will automatically detect `user.home\.soapui` and copy the installation files to `user.home\.soapui\\plugins` and `user.home\dragonfly` folders.

---

> **Installation Directory for soapUI 5.0x and soapUI 4.x**
>
> In the legacy plugin architecture, soapUI requires that all plugins are installed in 
>
> `<soapui installation folder>\bin\plugins`
>
> **example:** `C:\\Program Files\SmartBear\SoapUI-Pro-4.6.4\bin\plugins`
>
> The dragonfly install will ask the user to specify the location of the soapUI installation.


