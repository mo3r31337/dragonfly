Installation
============

Installation requirements for Dragonfly ver. 6.0

---

**Requirements**

* soapui 5.12 or higher
* HP ALM 10, 11, 12, or SAAS
* Windows OS
* Install HP Quality Center Connectivity Add-in from ALM management tools. Typically <code>http://<alm url>:8080/qcbin/addins.html</code>

----

![](../img/setup/setup_1.png)

---

**Run the Dragonfly installer**

![](../img/setup/setup_2.png)

![](../img/setup/setup_3.png)

![](../img/setup/setup_4.png)

![](../img/setup/setup_5.png)

![](../img/setup/setup_6.png)

![](../img/setup/setup_7.png)



