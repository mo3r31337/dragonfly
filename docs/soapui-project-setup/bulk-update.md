Bulk Update
===========

By default, Dragonfly updates test results in real time. This means after each test ends in soapUI, Dragonfly will connect with HP ALM, upload test results, and then disconnect from ALM.

A project with very large number of tests or teams with slow connection to HP ALM, may experience some slowness due to real time updates. 

If this performance is an issue, Dragonfly Bulk Update feature allows you to update all tests at the end of the test run. This approach results in just one connection to HP ALM, mass updates to tests, and then a disconnection from ALM.

> **HP ALM Performance Analysis**
> 
> In HP ALM, the most expensive operation is authentication. Authentication alone can take 1000-2000 ms. 
> Bulk updates addresses this problem by authenticating only once.

To use bulk update feature, follow these two steps.

**Step 1**. Change `QC_BulkUpdate` project property to `enabled`

![](../img/soapui_project_setup/2015-06-08_23h59_26.png)

**Step 2**. Run tests in soapUI as usual. You will see message in the dragonfly log tab indicating that bulkupdate is enabled and test results are being written to file.

![](../img/soapui_project_setup/2015-06-09_00h01_23.png)

**Step 3**. Select `Dragonfly: Bulk Results Update` project menu. Dragonfly will now process all the test results and upload them to HP ALM.

![](../img/soapui_project_setup/2015-06-09_00h03_36.png)

