Custom Properties Configuration
===============================

In many ALM setups, users define custom user fields in various project entities such as Test, TestSets, Test Runs, etc. In such a setup, your ALM tests may have extra fields like in this screenshot

![](../img/soapui_project_setup/2015-06-22_23h32_05.png)

Dragonfly 6 makes it super easy to configure custom user fields by using a configuration wizard.

> **Error Message for non-configured projects**
>
> If an ALM project has `required` user properties and the soapUI project has not mapped these user properties using the Dragonfly configuration wizard, you will see errors in the Dragonfly log tab during most of the operations. 
> ![](../img/soapui_project_setup/2015-06-22_23h30_46.png)

To configure custom user fields, simple right click on your project and select `Dragonfly: Configure Custom Fields`. This will open the custom fields configuration wizard.

![](../img/soapui_project_setup/2015-06-22_23h36_00.png)

The wizard allows you to configure `Test`, `TestSet` and `Run` project entities. Simply specify the ALM Field Name, Label, and default value. Click <code>Next</code> to configure other entities or click <code>save</code> to save the mappings.

![](../img/soapui_project_setup/2015-06-22_23h38_13.png)

Once you click `save`, the default values for each of the custom properties will be added to the soapUI project in the following places

* `Test:` soapUI TestCase custom property 
* `Testset:` soapUI project and test suite custom property
* `Run:` soapUI  TestCase custom property

----

`TestCase Example:`

![](../img/soapui_project_setup/2015-06-23_00h14_56.png)

