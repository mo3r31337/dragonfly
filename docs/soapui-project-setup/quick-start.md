Quick Start
===========

Follow these steps to get up and running in 10 minutes

---

**Step 1.** Start soapUI and lets configure your first project. The highlighted properties below are required and must be configured to reflect your ALM environment settings. 

![](../img/soapui_project_setup/2015-06-06_14h23_24.png)

**Step 2. **Export soapUI project into ALM. Simply right click the project and select <code>Dragonfly: Export to HP ALM</code>. Dragonfly plugin will show a progress bar and live export status in the Dragonfly tab.

![](../img/soapui_project_setup/2015-06-08_08h09_36.png)

> **Faster Exports**
>
> Export to ALM action can also be invoked from the soapUI testsuite or testcase. Project action will export/update the entire project. Whereas, testsuite or testcase level actions will only export/update the specified testsuite and testcase respectively.

![](../img/soapui_project_setup/2015-06-06_14h34_10.png)

> **Intelligent Export**
>
> You don't have to worry about manually creating any of the QC_TestPlanDirectory, QC_TestLabDirectory, or QC_TestSet. Dragonfly is smart enough to figure this out and create it automatically at run time.

**Step 3.** The soapUI tests should now be visible in the specified TestPlan directory. 

![](../img/soapui_project_setup/2015-06-06_14h55_27.png)

**Step 4a. **Run tests from soapUI.

![](../img/soapui_project_setup/2015-06-08_08h19_58.png)

**Step 4b.** Or run tests from HP ALM Testlab/TestSet

To run the tests from HP ALM Testlab, create a testset and add tests cases to the testset just like you would when running HP UFT, QTP, etc. 

> **HP ALM Host setup requirements**
>
> When running soapUI tests from HP ALM, the tests will run on some set of hosts selected by the user. The user must make sure that these hosts have all the necessary setup: soapUI, dragonfly, licenses, and the soapUI project.

> **Specify TestLab path and Testset name in project custom properties**
>
> Make sure that the path you create in Testlab is the same path that is specified in your soapUI project properties.

![](../img/soapui_project_setup/2015-06-21_23h32_39.png)

![](../img/soapui_project_setup/2015-06-21_23h40_12.png)

**Step 5. **Viewing Test Results in HP ALM TestLab.

Go to the appropriate TestLab and TestSet. This page will show you last soapUI run summary.

![](../img/soapui_project_setup/2015-06-22_06h21_04.png)

To view run details or to view run history, click on the testcase in the testset and select <code>Runs</code>

![](../img/soapui_project_setup/2015-06-22_06h21_04.png)

The run view shows all the runs for this test case. It is useful to see why a test case started failing. Click on the failing <code>Run ID</code> to view the request and response for the various test steps.

![](../img/soapui_project_setup/2015-06-22_06h32_48.png)

![](../img/soapui_project_setup/2015-06-22_06h40_03.png)

