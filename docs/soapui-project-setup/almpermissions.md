ALM Permissions
===============

Dragonfly plugin creates tests and uploads test results in HP ALM's TestPlan and TestLab. As such, the user needs to have appropriate permission in HP ALM.

*TestPlan*

- **Create** TestPlan folder
- **Create** Tests in TestPlan
- **Create, Update, Delete** Design Steps in the testcase

![](../img/soapui_project_setup/testplan-permissions.png)

*TestLab*

- **Create, Update, Delete** Result
- **Create, Update, Delete** Run
- **Create, Update, Delete** Run Step
- **Create, Update  ** Test Instance 
- **Create, Update ** Test Set 
- **Create, Update** TestLab folder

![](../img/soapui_project_setup/testlab-permissions-1.png)

