Import Tests from ALM
=====================

In some organizations, it is very common that teams do the initial test design in ALM Test Plan. Once the application is ready, the team will write automated tests using a variety of testing tools like Selenium, soapUI, etc

Dragonfly plugin allows you to import tests from ALM Test Plan into a soapUI project and create a skeleton project to help you get started. 

Once the user has added test steps in the skeleton testcase in soapUI, he/she will export the test case back into HP ALM. This way the team converts a simple testcase designs into fully automated test cases.

> **warning: Exporting will delete all design steps in HP ALM**
>
> "Dragonfly's Export action will always delete the design steps in ALM test case and re-add soapUI's test steps. This is done to ensure that the soapUI test is in sync with HP ALM testcase.

To import a test cases from HP ALM, simply configure the `QC_TestPlanDirectory` property in your soapUI new or existing project and select `Dragonfly: Import from HP ALM`. This will create testsuites and testcases in your soapUI project.

![](../img/soapui_project_setup/2015-06-23_00h29_38.png)

![](../img/soapui_project_setup/2015-06-23_00h52_03.png)

> **ALM Folder Hierarchy**
> 
> Dragonfly Import action creates soapUI test suites based on folders in ALM TestPlan. `QC_TestPlanDirectory` represents the root folder during the import process. The import process expects only one level of child folders and all the tests to be in these child folders. The child folders map to test suites in soapUI. If tests in the ALM Test Plan are inside the root folder or if the child folders have other child folders, the import process cannot create the appropriate structure in soapUI. Instead, all the tests from `QC_TestPlanDirectory` are imported into a generic test suite `ALM_Imported_TestCases`

![](../img/soapui_project_setup/2015-06-23_01h17_27.png)

![](../img/soapui_project_setup/2015-06-23_01h17_40.png)

