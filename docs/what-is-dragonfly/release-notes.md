Release Notes
=============

* [new] Support for HP ALM 12 and SAAS.
* [new] Support for ReadyAPI 1.3.0 and ReadyAPI 1.4.0.
* [new] Brand new installer with smarter diagnostics and configuration wizards.
* [new] ALM custom user properties configuration wizard.
* [new] User credentials over ride feature.
* [new] auto-save project when changing any ALM properties.
* [new] New product website.
* [new] New user guide portal.
* [new] Agiletestware testing blogs.
* [new] Dragonfly on boarding videos
* [new] Improved logging 
* [new] Improved diagnostics 
* [new] Improved install experience
* [new] Major rewrite of the plugin by removing dependency from .NET code.
* [bug] Installer bugs with %homepath% and %userprofile% install location logic.
* [bug] Dragonfly log tab might not show up under certain conditions.
* [bug] Correct error message when ALM client is incorrect or missing
