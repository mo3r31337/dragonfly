Licenses
========

**What is the license model for Agiletestware Dragonfly?**

Dragonfly is only sold as a site license. This means that there are no restrictions on the number of users. The price for 1 user vs. 1000 users is the same. The same license can be shared and installed on any computer within the organization.

**How can I get formal quote or invoice for Agiletestware?**

 Please send your request to [Agiletestware Sales](https://agiletestware.wufoo.com/forms/agiletestware-contact-us/ "Contact Sales")  and we can generate a formal invoice for your purchasing department. 

**Can I purchase Dragonfly using a Purchase Order?**

Yes. Please send your PO to [Agiletestware Sales](https://agiletestware.wufoo.com/forms/agiletestware-contact-us/ "Contact Sales") 

**I just placed an order and did not get my license key. How do I get the license key ?**

It usually takes 12-30 hours for the keys to be generated and sent via email. Please contact  [Agiletestware Sales](https://agiletestware.wufoo.com/forms/agiletestware-contact-us/ "Contact Sales") if you have not received your keys.

**Does Agiletestware work with resellers ?**

We work with various resellers. We currently work with SHI, SoftChoice, and Acquion.
